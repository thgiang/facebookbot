<?php

/**
 * This file select all conversations from database and save to conversations.corpus.json
 */
require_once 'config.php';
$servername = "localhost";
$username = "root";
$password = "password";
$dbname = "facebook";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

$sql = "SELECT * FROM conversations";
$result = $conn->query($sql);

if ($result->num_rows > 0)
{
    $fileContent['conversations'] = array();
    while ($row = $result->fetch_assoc())
    {
        $content = json_decode($row['content']);
        if (!empty($content))
        {
            $conversation = array();
            foreach ($content AS $qa)
            {

                if ($qa->q != '' AND $qa->a != '')
                {
                    if (isset($_GET['rmvn']) AND !empty($_GET['rmvn']))
                    {
                        $conversation[] = remove_vietnamese_character($qa->q);
                        $conversation[] = remove_vietnamese_character($qa->a);
                    } else
                    {
                        $conversation[] = $qa->q;
                        $conversation[] = $qa->a;
                    }

                }
            }
            $fileContent['conversations'][] = $conversation;
        }
    }

    //Save to file.
    if (isset($_GET['rmvn']) AND !empty($_GET['rmvn']))
    {
        file_put_contents('conversations_rmvn.corpus.json', json_encode($fileContent));
        echo 'Done.';
    } else
    {
        file_put_contents('conversations.corpus.json', json_encode($fileContent));
        echo 'Done. If you want to remove all VietNamese character, use: gen_corpus.php?rmvn=true';
    }
}

function remove_vietnamese_character($str)
{
    $unicode = array('a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ', 'd' => 'đ', 'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ', 'i' => 'í|ì|ỉ|ĩ|ị', 'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ', 'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự', 'y' => 'ý|ỳ|ỷ|ỹ|ỵ', 'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ', 'D' => 'Đ', 'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ', 'I' => 'Í|Ì|Ỉ|Ĩ|Ị', 'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ', 'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự', 'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',);

    foreach ($unicode as $nonUnicode => $uni)
    {
        $str = preg_replace("/($uni)/i", $nonUnicode, $str);
    }

    return $str;
}