# About #

I am using PHP and Python to build Facebook BOT (FBT). Firstly, it crawls data from Fanpage to train ChatterBOT. After that, FBT knows how to answer customer 's question and reply them automatically.
This is my Graduation Project in Faculty of Information Technology - Viet Nam National University of Agriculture (VNUA). 

# How do I get set up? #

Because this application is written by PHP and Python, you must set up the appropriate environment. 
For example, I am using Windows 10 so I have installed Xampp and Python for Windows. 

Step 1: Open Xampp, start Apache and MySQL

Step 2: Go to localhost/phpmyadmin then create a new database named "facebook" and import database.sql

Step 3: Go to https://developer.facebook.com to register a new Facebook Application then copy the Application ID, Secret Key

Step 4: Open file config.php and edit config

Done!

You can test by running index.php, it will crawl all conversations and save to database automatically. The crawl speed depends on your connection, and it will run to die :)). You can force stop it by closing your browser.

After that, you run gen_corpus.php to generate corpus file named conversations.corpus.json (in the same folder). The corpus file is used to train the ChatterBOT.