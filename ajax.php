<?php
require_once 'config.php';

//Create connection
$conn = new mysqli(DB_SERVER_NAME, DB_USER_NAME, DB_PASS, DB_NAME);

//Get 1 post per request (may be higher by editing ?limit=2 on both 2 lines under)
if (!isset($_GET['until']) OR empty($_GET['until']))
{
    $feed = $fb->get(FP_ID . '/feed?limit=1', AC);
} else
{
    $feed = $fb->get(FP_ID . '/feed?until=' . $_GET['until'] . '&limit=1&__paging_token=' . $_GET['paging_token'] . '', AC);
}

//Get Facebook Fanpage Feed
$posts = $feed->getDecodedBody();
if (!empty($posts))
{
    foreach ($posts['data'] AS $post)
    {
        //Get first 100 comments
        $cmtQuery = $fb->get($post['id'] . '/comments?limit=100', AC);
        $comments = $cmtQuery->getDecodedBody();
        if (!empty($comments))
        {
            foreach ($comments['data'] AS $comment)
            {
                if ($comment['from']['id'] != FP_ID)
                {
                    $conversation = array();
                    //Each comment, get first 30 sub-comment (replying the comment is called by "sub-comment")
                    $subCmtQuery = $fb->get($comment['id'] . '/comments?limit=30', AC);
                    $subComments = $subCmtQuery->getDecodedBody();

                    //Flag if a customer is waiting for reply from Admin
                    $waiting = true;
                    $qWaiting = array('q' => $comment['message']);
                    foreach ($subComments['data'] AS $subComment)
                    {
                        if ($subComment['from']['id'] == FP_ID)
                        {
                            //Find reply from Admin
                            if ($waiting AND !empty($qWaiting))
                            {

                                if (!empty($qWaiting['q']) AND !empty($subComment['message']))
                                {
                                    $qa = array('q' => $qWaiting['q'], 'a' => $subComment['message'],);
                                    $conversation[] = $qa;

                                    //Flag customer is waiting = false
                                    $qWaiting = array();
                                    $waiting = false;
                                }
                            }
                        }

                        //If customer continue post a sub-comment, flag customer is waiting = true
                        if ($subComment['from']['id'] == $comment['from']['id'])
                        {
                            $waiting = true;
                            $qWaiting = array('q' => $subComment['message']);
                        }
                    }

                    //Save conversation to database
                    if (!empty($conversation))
                    {
                        $content = $conn->real_escape_string(json_encode($conversation));
                        $sql = "INSERT INTO conversations (content) VALUES ('{$content}')";
                        $conn->query($sql);
                    }
                }
            }

        }
    }

    if (isset($posts['paging']['next']) AND !empty($posts['paging']['next']))
    {
        $parts = parse_url($posts['paging']['next']);
        parse_str($parts['query'], $query);
        echo json_encode($query);
    }
}
else
{
    echo json_encode(array('until' => 'end'));
}	
