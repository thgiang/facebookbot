﻿<?php
session_start();
session_destroy();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>576765 - Zang BOT</title>

    <!-- Bootstrap -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="style/js/bootstrap.min.js"></script>

    <script src="style/js/js.cookie.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<h1>Getting data from Facebook.</h1>
Background process is running. You can open Network tab on Debug tools to see more.</h1>
<script>
    $(document).ready(function () {
        if (typeof Cookies.get('until') == 'undefined') {
            Cookies.set('until', '');
            Cookies.set('paging_token', '');
        }
        function run(until, paging_token) {
            $.ajax({
                url: 'ajax.php',
                type: 'GET',
                dataType: 'json',
                data: {'until': until, 'paging_token': paging_token},
                success: function (res) {
                    if (res.until != '0') {
                        Cookies.set('until', res.until);
                        Cookies.set('paging_token', res.__paging_token);
                        run(Cookies.get('until'), Cookies.get('paging_token'));
                        console.log(res);
                    }
                    else {
                        alert("End");
                    }
                },
                error: function (data) {
                    run(Cookies.get('until'), Cookies.get('paging_token'));
                },
            });
        }

        run(Cookies.get('until'), Cookies.get('paging_token'));

    });
</script>
</body>
</html>